#pragma once

#include <memory>

#include <terragear/tg_light.hxx>
#include <terragear/tg_polygon.hxx>


class LightingObj
{
public:
    explicit LightingObj(char* def);

    double lat {-9999};
    double lon {-9999};
    int type;
    double heading;
    double glideslope;
    char assoc_rw;
    bool valid {true};


    void BuildBtg(tglightcontour_list& lights);
};

typedef std::vector<std::shared_ptr<LightingObj>> LightingObjList;
