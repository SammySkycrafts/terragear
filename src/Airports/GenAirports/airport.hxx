#pragma once

#include <memory>

#include <simgear/math/sg_types.hxx>
#include <simgear/threads/SGThread.hxx>
#include <simgear/timing/timestamp.hxx>

#include "closedpoly.hxx"
#include "debug.hxx"
#include "helipad.hxx"
#include "linearfeature.hxx"
#include "linked_objects.hxx"
#include "object.hxx"
#include "runway.hxx"
#include "taxiway.hxx"


class Airport
{
public:
    Airport(int c, char* def);
    ~Airport();

    void AddRunway(std::shared_ptr<Runway> runway)
    {
        runways.push_back(runway);
    }

    void AddWaterRunway(std::shared_ptr<WaterRunway> runway)
    {
        waterRunways.push_back(runway);
    }

    void AddObj(std::shared_ptr<LightingObj> lightObj)
    {
        lightObjects.push_back(lightObj);
    }

    void AddHelipad(std::shared_ptr<Helipad> helipad)
    {
        helipads.push_back(helipad);
    }

    void AddTaxiway(std::shared_ptr<Taxiway> taxiway)
    {
        taxiways.push_back(taxiway);
    }

    void AddPavement(std::shared_ptr<ClosedPoly> pavement)
    {
        pavements.push_back(pavement);
    }

    void AddFeature(std::shared_ptr<LinearFeature> feature)
    {
        features.push_back(feature);
    }

    void AddFeatures(FeatureList feature_list)
    {
        for (auto& feature : feature_list) {
            features.push_back(feature);
        }
    }

    int NumFeatures() const
    {
        return features.size();
    }

    void AddBoundary(std::shared_ptr<ClosedPoly> boundary)
    {
        boundaries.push_back(boundary);
    }

    void AddWindsock(std::shared_ptr<Windsock> windsock)
    {
        windsocks.push_back(windsock);
    }

    void AddBeacon(std::shared_ptr<Beacon> beacon)
    {
        beacons.push_back(beacon);
    }

    void AddSign(std::shared_ptr<Sign> sign)
    {
        signs.push_back(sign);
    }

    std::string GetIcao() const
    {
        return icao;
    }

    void GetBuildTime(SGTimeStamp& tm)
    {
        tm = build_time;
    }

    void GetTriangulationTime(SGTimeStamp& tm)
    {
        tm = triangulation_time;
    }

    void GetCleanupTime(SGTimeStamp& tm)
    {
        tm = cleanup_time;
    }

    void merge_slivers(tgpolygon_list& polys, tgcontour_list& slivers);
    void BuildBtg(const std::string& root, const string_list& elev_src);

    void set_debug(std::string& path,
                   debug_map& dbg_runways,
                   debug_map& dbg_pavements,
                   debug_map& dbg_taxiways,
                   debug_map& dbg_features)
    {
        debug_path = path;
        debug_runways = dbg_runways;
        debug_pavements = dbg_pavements;
        debug_taxiways = dbg_taxiways;
        debug_features = dbg_features;
    };

    bool isDebugRunway(int) const;
    bool isDebugPavement(int) const;
    bool isDebugTaxiway(int) const;
    bool isDebugFeature(int) const;

private:
    bool CheckZFightingTriangles(const std::string& prefix, const std::string& debug_root, const tgPolygon& base_poly, const tgpolygon_list& rwy_polys, const tgpolygon_list& pvmt_polys);

    int code;                // airport, heliport or sea port
    int altitude;            // in meters
    std::string icao;        // airport code
    std::string description; // description

    PavementList pavements;
    FeatureList features;
    RunwayList runways;
    WaterRunwayList waterRunways;
    TaxiwayList taxiways;
    LightingObjList lightObjects;
    WindsockList windsocks;
    BeaconList beacons;
    SignList signs;
    HelipadList helipads;
    PavementList boundaries;

    // stats
    SGTimeStamp build_time;
    SGTimeStamp cleanup_time;
    SGTimeStamp triangulation_time;

    // debug
    std::string debug_path;
    debug_map debug_runways;
    debug_map debug_pavements;
    debug_map debug_taxiways;
    debug_map debug_features;
};

typedef std::vector<std::shared_ptr<Airport>> AirportList;
