#pragma once

#include <memory>

#include <terragear/tg_polygon.hxx>


class Windsock
{
public:
    explicit Windsock(char* def);

    double lat {-9999};
    double lon {-9999};
    int lit;
    bool valid {true};

    SGGeod GetLoc() const
    {
        return SGGeod::fromDeg(lon, lat);
    }

    bool IsLit() const
    {
        return (lit == 1) ? true : false;
    }
};

typedef std::vector<std::shared_ptr<Windsock>> WindsockList;


class Beacon
{
public:
    explicit Beacon(char* def);

    double lat {-9999};
    double lon {-9999};
    int code;
    bool valid {true};

    SGGeod GetLoc() const
    {
        return SGGeod::fromDeg(lon, lat);
    }

    int GetCode() const
    {
        return code;
    }
};

typedef std::vector<std::shared_ptr<Beacon>> BeaconList;

class Sign
{
public:
    explicit Sign(char* def);

    double lat {-9999};
    double lon {-9999};
    double heading;
    int reserved;
    int size;
    std::string sgn_def;
    bool valid {true};

    SGGeod GetLoc() const
    {
        return SGGeod::fromDeg(lon, lat);
    }

    double GetHeading() const
    {
        return heading;
    }

    std::string GetDefinition() const
    {
        return sgn_def;
    }

    int GetSize() const
    {
        return size;
    }
};

typedef std::vector<std::shared_ptr<Sign>> SignList;
